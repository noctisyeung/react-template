import React from 'react';

const Loading = () => (
  <div id="loading">
    <div className="loading-wrapper">
      <div className="loader" />
      <h1>LOADING...</h1>
    </div>
  </div>
);

export default Loading;
