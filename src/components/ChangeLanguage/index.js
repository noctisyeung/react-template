import React from 'react';
import { useTranslation } from 'react-i18next';

const ChangeLanguage = () => {
  const { i18n } = useTranslation();
  const [lang, setLang] = React.useState('en');

  const _handleSelectLanguage = (e) => {
    setLang(e.target.value);
  };

  const _handleChangeLanguage = () => {
    i18n.changeLanguage(lang);
  };

  return (
    <div className="change-lang-wrapper">
      <label htmlFor="change-lang-select">
        Languages:
        <select
          id="change-lang-select"
          onChange={_handleSelectLanguage}
          value={lang}
        >
          <option value="en">English</option>
          <option value="hk">中文</option>
        </select>
      </label>
      <button type="button" onClick={_handleChangeLanguage}>
        Change
      </button>
    </div>
  );
};

export default ChangeLanguage;
