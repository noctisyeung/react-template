import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import isAuth from '../utils/isAuth';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    /* eslint-disable react/jsx-props-no-spreading */
    {...rest}
    render={(props) =>
      isAuth() ? <Component {...props} /> : <Redirect to="/login" />
    }
  />
);

PrivateRoute.propTypes = {
  component: PropTypes.element,
};

PrivateRoute.defaultProps = {
  component: PropTypes.element,
};

export default PrivateRoute;
