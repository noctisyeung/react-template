import React from 'react';
import { Switch, BrowserRouter, Route, Redirect } from 'react-router-dom';
import Loading from '../components/Loading';

const PrivateRoute = React.lazy(() => import('./PrivateRoute'));
const Home = React.lazy(() => import('../views/Home'));
const NotFound = React.lazy(() => import('../views/NotFound'));

const Router = () => (
  <BrowserRouter basename="/">
    <React.Suspense fallback={<Loading />}>
      <Switch>
        <PrivateRoute exact path="/" component={Home} />
        <Route path="/en">
          <Redirect to={`${window.location.pathname.replace('/en', '')}`} />
        </Route>
        <Route component={NotFound} />
      </Switch>
    </React.Suspense>
  </BrowserRouter>
);

export default Router;
