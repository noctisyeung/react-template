const isAuth = () => {
  let isLogin = false; // Initial status is not Login
  const currentUserStatus = localStorage.getItem('User');
  isLogin = !(currentUserStatus === null || currentUserStatus === undefined);
  return isLogin;
};

export default isAuth;
