import { combineReducers } from 'redux';
import LoadingReducer from './Loading';

const ReducerStore = combineReducers({
  LoadingReducer,
});

export default ReducerStore;
