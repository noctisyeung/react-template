import * as type from '../type';

const updateLoadingStatus = (isLoading) => {
  const state = {
    loading: false,
  };
  state.loading =
    isLoading === state.data.loading ? state.data.loading : isLoading;
  return {
    type: type.UPDATE_LOADING_ACTION,
    data: state,
  };
};

export default updateLoadingStatus;
