import * as type from '../type';

const initState = {
  loading: true,
};

const LoadingReducer = (state = initState, action) => {
  const newState = { ...state };
  switch (action.type) {
    case type.UPDATE_LOADING_ACTION:
      if (action.data.loading !== newState.loading) {
        newState.loading = action.data.loading;
      }
      return newState;
    default:
      return state;
  }
};

export default LoadingReducer;
