import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import ReducerStore from './redux';
import Router from './routers';
import './i18n';
import './assets/css/main.scss';
import * as serviceWorker from './serviceWorker';

const middleware = [];

if (
  process.env.NODE_ENV === undefined ||
  process.env.NODE_ENV !== 'production'
) {
  // eslint-disable-next-line global-require
  const { createLogger } = require('redux-logger');
  const logger = createLogger({
    duration: true,
    timestamp: true,
  });
  middleware.push(logger);
}

const store = createStore(ReducerStore, applyMiddleware(...middleware));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
