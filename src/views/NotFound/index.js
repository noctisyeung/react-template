import React from 'react';
import { useTranslation } from 'react-i18next';

const NotFound = () => {
  const { t } = useTranslation(['notFound']);
  return (
    <div id="not-found">
      <div className="not-found-wrapper">
        <h1>404</h1>
        <h4>{t('notFoundOops')}</h4>
        <p>{t('notFoundDetail')}</p>
      </div>
    </div>
  );
};

export default NotFound;
