import React from 'react';
import { useTranslation } from 'react-i18next';
import ChangeLanguage from '../../components/ChangeLanguage';

const Home = () => {
  const { t } = useTranslation(['home']);
  return (
    <div id="home">
      <h1>{t('welcomeMsg')}</h1>
      <ChangeLanguage />
    </div>
  );
};

export default Home;
