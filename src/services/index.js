import axios from 'axios';

const rootApi = axios.create({
  baseURL: '/',
});

const getRoot = rootApi.get();

export default getRoot;
